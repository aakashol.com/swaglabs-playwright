const fs = require('fs');

/**
* @param {string} env current environment
* @param {string} module to find the correct user
* @param {string} username to run the tests with
* @returns {object} the credentials selected.
 */
export default function credentialsProvider(env, product, username) {
  const allCredentials = JSON.parse(fs.readFileSync('logins.json'));

  const creds = allCredentials[`${env}`][`${product}`].find((obj) => obj.username === `${username}`);
  return creds;
}
