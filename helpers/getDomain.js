const fs = require('fs');

/**
* @param {string} env current environment
* @param {string} module to find the correct user
* @param {string} email to run the tests with
* @returns {object} the credentials selected.
 */
export default function getDomain(env, product) {
  const details = JSON.parse(fs.readFileSync('environment-domain-map.json'));

  const { domain } = details[`${env}`][`${product}`];
  return domain;
}
