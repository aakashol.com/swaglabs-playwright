/**
 * This function removed the $ sign from the string and cparses the remainder of the string to integer or number
 * @param {string} amount is in the form of string with $ as prefix
 * @returns {number} amount in the form of number
 */
export default function getAmount(amount) {
  const temp = amount.split('$');
  return parseFloat(temp[1]);
}
