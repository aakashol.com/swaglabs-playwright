/**
 * @param {Array} ar - array that needs to be sorted
 * @returns {Array} Sorted array
 */
export default function sortArrayOfNumbers(a, b) {
  return a > b ? 1 : b > a ? -1 : 0;
}
