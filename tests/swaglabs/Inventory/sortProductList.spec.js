import { firefox } from 'playwright';
import { assert } from 'chai';
import getDomain from '../../../helpers/getDomain';
import credentialsProvider from '../../../helpers/credentialsProvider';
import sortArrayOfNumbers from '../../../helpers/sortArrayOfNumbers';

const global = require('../../../temp/globals.json');
const { LoginPage } = require('../../../pages/swaglabs/login');
const { InventoryPage } = require('../../../pages/swaglabs/inventory');

let page;
let browser;

describe('Test sorting this product list', () => {
  let loginPage;
  const domain = getDomain(global.environment, global.product);
  const credentials = credentialsProvider(
    global.environment,
    global.product,
    'standard_user',
  );

  before('Open browser', async () => {
    browser = await firefox.launch({
      headless: true,
      ignoreDefaultArgs: ['--mute-audio'],
      slowMo: 200,
    });
    const context = await browser.newContext({
      ignoreHTTPSErrors: true,
      recordVideo: { dir: 'videos/' },
    });
    page = await context.newPage();

    loginPage = new LoginPage(page);
    await loginPage.navigate(domain);
  });

  after(() => {
    if (!page.isClosed()) {
      browser.close();
    }
  });

  it('User should be able to login', async () => {
    await loginPage.login(credentials.username, credentials.password);
  });

  it('Verify sorting by name (A-Z)', async () => {
    const inventoryPage = new InventoryPage(page);
    let listOfProducts = await inventoryPage.getListOfProducts();
    listOfProducts = listOfProducts.sort();
    const sortedListOfProducts = await inventoryPage.getListOfProducts();
    assert.deepEqual(sortedListOfProducts, listOfProducts);
  });

  it('Verify sorting by name (Z-A)', async () => {
    const inventoryPage = new InventoryPage(page);
    const listOfProducts = await inventoryPage.getListOfProducts();
    listOfProducts.reverse();
    await inventoryPage.sortByNameZtoA();
    const sortedListOfProducts = await inventoryPage.getListOfProducts();
    assert.deepEqual(sortedListOfProducts, listOfProducts);
  });

  it('Verify sorting by Price (Low to High)', async () => {
    const inventoryPage = new InventoryPage(page);
    const listOfProducts = await inventoryPage.getListOfPrices();
    listOfProducts.sort(sortArrayOfNumbers);
    await inventoryPage.sortByPriceLowToHigh();
    const sortedListOfProducts = await inventoryPage.getListOfPrices();
    assert.deepEqual(sortedListOfProducts, listOfProducts);
  });

  it('Verify sorting by Price (High-Low)', async () => {
    const inventoryPage = new InventoryPage(page);
    const listOfProducts = await inventoryPage.getListOfPrices();
    listOfProducts.reverse();
    await inventoryPage.sortByPriceHighToLow();
    const sortedListOfProducts = await inventoryPage.getListOfPrices();
    assert.deepEqual(sortedListOfProducts, listOfProducts);
  });
});
