import playwright from 'playwright';
import { assert } from 'chai';
import getDomain from '../../../helpers/getDomain';
import credentialsProvider from '../../../helpers/credentialsProvider';

const { LoginPage } = require('../../../pages/swaglabs/login');
const global = require('../../../temp/globals.json');

let page;
let browser;

['chromium', 'firefox', 'webkit'].forEach((browserType) => {
  describe(`Test Login in ${browserType} browser`, () => {
    let loginPage;
    const domain = getDomain(global.environment, global.product);
    const credentials = credentialsProvider(
      global.environment,
      global.product,
      'standard_user',
    );

    before(`Open ${browserType} browser`, async () => {
      browser = await playwright[browserType].launch({ headless: true });
      const context = await browser.newContext();
      page = await context.newPage();

      loginPage = new LoginPage(page);
      await loginPage.navigate(domain);
    });

    after(() => {
      if (!page.isClosed()) {
        browser.close();
      }
    });

    it('Check Username error', async () => {
      await loginPage.clickLogin();
      await page.waitForSelector('data-test=error');
      const errorMsg = await page.innerText('data-test=error');
      assert.strictEqual(errorMsg, 'Epic sadface: Username is required');
    });

    it('Check Password error', async () => {
      await loginPage.setUsername(credentials.username);
      await loginPage.clickLogin();
      const errorMsg = await page.innerText('data-test=error');
      assert.strictEqual(errorMsg, 'Epic sadface: Password is required');
    });

    it('Check Username and Password combination mismatch error', async () => {
      await loginPage.setPassword('test');
      await loginPage.clickLogin();
      const errorMsg = await page.innerText('data-test=error');
      assert.strictEqual(
        errorMsg,
        'Epic sadface: Username and password do not match any user in this service',
      );
    });

    it('Check locked out user error', async () => {
      const lockedUser = credentialsProvider(
        'dev',
        'swaglabs',
        'locked_out_user',
      );
      await loginPage.clearUsername();
      await loginPage.setUsername(lockedUser.username);
      await loginPage.setPassword(lockedUser.password);
      await loginPage.clickLogin();
      const errorMsg = await page.innerText('data-test=error');
      assert.strictEqual(
        errorMsg,
        'Epic sadface: Sorry, this user has been locked out.',
      );
    });

    it('User should be able to login', async () => {
      await loginPage.login(credentials.username, credentials.password);
      assert.strictEqual(await page.url(), `${domain}/inventory.html`);
    });
  });
});
