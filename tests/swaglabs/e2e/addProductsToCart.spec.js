import { firefox } from 'playwright';
import { assert } from 'chai';
import getDomain from '../../../helpers/getDomain';
import credentialsProvider from '../../../helpers/credentialsProvider';

const faker = require('faker');
const global = require('../../../temp/globals.json');
const { LoginPage } = require('../../../pages/swaglabs/login');
const { InventoryPage } = require('../../../pages/swaglabs/inventory');
const { CartPage } = require('../../../pages/swaglabs/cart');
const { CheckoutPage } = require('../../../pages/swaglabs/checkout');
const {
  CheckoutOverviewPage,
} = require('../../../pages/swaglabs/checkoutOverview');
const { Header } = require('../../../pages/swaglabs/header');

let page;
let browser;

describe('Test Adding product to cart', () => {
  console.log(`global: ${JSON.stringify(global)}`);
  console.log(`global.environment: ${global.environment}`);
  console.log(`global.product: ${global.product}`);
  let loginPage;
  const domain = getDomain(global.environment, global.product);
  const products = [
    'Sauce Labs Backpack',
    'Sauce Labs Bike Light',
    'Sauce Labs Bolt T-Shirt',
  ];
  const credentials = credentialsProvider(
    global.environment,
    global.product,
    'standard_user',
  );
  let productPrices;
  const firstName = faker.name.firstName();
  const lastName = faker.name.lastName();
  const zipCode = faker.address.zipCode();

  before('Open browser', async () => {
    browser = await firefox.launch({
      headless: true,
      ignoreDefaultArgs: ['--mute-audio'],
      slowMo: 200,
    });
    const context = await browser.newContext({
      ignoreHTTPSErrors: true,
      recordVideo: { dir: 'videos/' },
    });
    page = await context.newPage();

    loginPage = new LoginPage(page);
    await loginPage.navigate(domain);
  });

  after(() => {
    if (!page.isClosed()) {
      browser.close();
    }
  });

  it('User should be able to login', async () => {
    await loginPage.login(credentials.username, credentials.password);
  });

  it('Add product(s) to cart', async () => {
    const inventoryPage = new InventoryPage(page);
    await inventoryPage.addToCart(products);
    productPrices = await inventoryPage.getProductPrice(products);
  });

  it('Go to shopping cart', async () => {
    const inventoryPage = new InventoryPage(page);
    await inventoryPage.clickShoppingCart();
    const numberofProductsInshoppingCart = parseInt(
      await inventoryPage.getNumberOfProductsInShoppingCart(),
      10,
    );
    assert.strictEqual(
      numberofProductsInshoppingCart,
      products.length,
      'Cart label shows correct number of products added to cart',
    );
  });

  it('Verify Product(s) and their prices in cart', async () => {
    const cartPage = new CartPage(page);
    const cartProductPrices = await cartPage.getProductPriceList();
    assert.deepEqual(
      cartProductPrices,
      productPrices,
      'Names and prices of products in list page should match with Names and prices of products in cart',
    );
  });

  it('Let us check out', async () => {
    const cartPage = new CartPage(page);
    await cartPage.clickCheckout();
    const checkoutPage = new CheckoutPage(page);
    await checkoutPage.fillYourInformation(firstName, lastName, zipCode);
    await checkoutPage.clickContinue();
  });

  it('Verify amounts on the checkout overview page', async () => {
    const checkoutOverview = new CheckoutOverviewPage(page);
    const itemTotalAmount = parseFloat(
      await checkoutOverview.getItemTotalAmount(),
    );
    const taxAmount = parseFloat(await checkoutOverview.getTaxAmount());
    const totalAmount = parseFloat(await checkoutOverview.getTotalAmount());
    let actualItemTotalAmount = 0;
    let actualTaxAmount = 0;
    let actualTotalAmount = 0;

    for (let i = 0; i < productPrices.length; i += 1) {
      actualItemTotalAmount += productPrices[i][1];
    }

    actualTaxAmount = parseFloat(((actualItemTotalAmount * 8) / 100).toFixed(2));
    actualTotalAmount = actualItemTotalAmount + actualTaxAmount;
    assert.strictEqual(actualItemTotalAmount, itemTotalAmount);
    assert.strictEqual(actualTaxAmount, taxAmount);
    assert.strictEqual(actualTotalAmount, totalAmount);

    await checkoutOverview.clickFinish();
    assert.strictEqual(await page.url(), `${domain}/checkout-complete.html`);
  });

  it('Logout of the application', async () => {
    const logout = new Header(page);
    await logout.clickHamburgerButton();
    await logout.clickLogout();
    assert.strictEqual(await page.url(), `${domain}/`);
  });
});
