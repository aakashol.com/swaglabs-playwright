class LoginPage {
  constructor(page) {
    this.page = page;
  }

  async navigate(domain) {
    const response = await this.page.goto(domain, {
      waitUntil: 'domcontentloaded',
    });
    return response;
  }

  async clickLogin() {
    await this.page.click('id=login-button');
  }

  async setUsername(username) {
    await this.page.fill('id=user-name', username);
  }

  async clearUsername() {
    await this.page.focus('id=user-name');
    await this.page.keyboard.down('Control');
    await this.page.keyboard.press('A');
    await this.page.keyboard.up('Control');
    await this.page.keyboard.press('Backspace');
  }

  async setPassword(password) {
    await this.page.fill('id=password', password);
  }

  async login(username, password) {
    await this.setUsername(username);
    await this.setPassword(password);
    await this.clickLogin();
    // await this.page.waitForNavigation();
  }
}

module.exports = { LoginPage };
