import getAmount from '../../helpers/getAmount';

class InventoryPage {
  constructor(page) {
    this.page = page;
  }

  /**
     * Adds a product to cart
     * @param {object} product - list of the products to add to cart
     */
  async addToCart(products) {
    products.forEach(async (product) => {
      await this.page.click(
        `//div[text()='${product}']/ancestor::div/following-sibling::div/button`,
      );
    });
  }

  /**
     * @param {object} product - list of the products to add to cart
     * @returns {Array} an array of product(s) & its price
     */
  async getProductPrice(products) {
    const productPriceMap = [];
    products.forEach(async (product) => {
      let price = await this.page.innerText(
        `//div[text()='${product}']/ancestor::div/following-sibling::div/div`,
      );
      price = getAmount(price);
      productPriceMap.push([product, price]);
    });
    return productPriceMap;
  }

  async clickShoppingCart() {
    await this.page.click('.shopping_cart_link');
  }

  async getNumberOfProductsInShoppingCart() {
    const numberOfProducts = await this.page.innerText(
      '.shopping_cart_badge',
    );
    return numberOfProducts;
  }

  async getListOfProducts() {
    const listOfProducts = [];
    let numberOfProducts = await this.page.$$('.inventory_item_name');
    numberOfProducts = numberOfProducts.length;
    for (let i = 1; i <= numberOfProducts; i += 1) {
      const name = await this.page.innerText(
        `(//div[@class='inventory_item_name'])[${i}]`,
      );
      listOfProducts.push(name);
    }
    return listOfProducts;
  }

  async getListOfPrices() {
    const listOfproducts = [];
    let numberOfProducts = await this.page.$$('.inventory_item_name');
    numberOfProducts = numberOfProducts.length;
    for (let i = 1; i <= numberOfProducts; i += 1) {
      let price = await this.page.innerText(
        `(//div[@class='inventory_item_price'])[${i}]`,
      );
      price = getAmount(price);
      listOfproducts.push(price);
    }
    return listOfproducts;
  }

  async sortByNameZtoA() {
    const sortDropdown = await this.page.$(
      'data-test=product_sort_container',
    );
    sortDropdown.selectOption('za');
  }

  async sortByPriceLowToHigh() {
    const sortDropdown = await this.page.$(
      'data-test=product_sort_container',
    );
    sortDropdown.selectOption('lohi');
  }

  async sortByPriceHighToLow() {
    const sortDropdown = await this.page.$(
      'data-test=product_sort_container',
    );
    sortDropdown.selectOption('hilo');
  }
}

module.exports = { InventoryPage };
