class CheckoutComplete {
  constructor(page) {
    this.page = page;
  }

  async getThankYouMessage() {
    const message = await this.page.innerText('.complete-header');
    return message;
  }

  async getSubMessage() {
    const message = await this.page.innerText('.complete-text');
    return message;
  }
}

module.exports = { CheckoutComplete };
