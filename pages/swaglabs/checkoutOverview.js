import getAmount from '../../helpers/getAmount';

class CheckoutOverviewPage {
  constructor(page) {
    this.page = page;
  }

  async getItemTotalAmount() {
    let total = await this.page.innerText('.summary_subtotal_label');
    total = getAmount(total.trim());
    return total;
  }

  async getTaxAmount() {
    let tax = await this.page.innerText('.summary_tax_label');
    tax = getAmount(tax.trim());
    return tax;
  }

  async getTotalAmount() {
    let total = await this.page.innerText('.summary_total_label');
    total = getAmount(total.trim());
    return total;
  }

  async clickFinish() {
    await this.page.click('id=finish');
  }

  async clickCancel() {
    await this.page.click('id=cancel');
  }
}

module.exports = { CheckoutOverviewPage };
