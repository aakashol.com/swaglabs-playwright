import getAmount from '../../helpers/getAmount';

class CartPage {
  constructor(page) {
    this.page = page;
  }

  /**
     * @param {object} product - list of the products to add to cart
     * @returns {Array} an array of product(s) & its price
     */
  async getProductPriceList() {
    const productPriceMap = [];
    let numberOfProducts = await this.page.$$('.inventory_item_name');
    numberOfProducts = numberOfProducts.length;
    for (let i = 1; i <= numberOfProducts; i += 1) {
      const name = await this.page.innerText(
        `(//div[@class='inventory_item_name'])[${i}]`,
      );
      let price = await this.page.innerText(
        `(//div[@class='inventory_item_price'])[${i}]`,
      );
      price = getAmount(price);
      productPriceMap.push([name, price]);
    }
    return productPriceMap;
  }

  /**
     * Clicks on check out button in cart
     */
  async clickCheckout() {
    await this.page.click('id=checkout');
  }

  /**
     * Clicks on continue shopping button in cart
     */
  async clickContinueShopping() {
    await this.page.click('id=continue-shopping');
  }
}

module.exports = { CartPage };
