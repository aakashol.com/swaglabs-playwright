class CheckoutPage {
  constructor(page) {
    this.page = page;
  }

  async fillYourInformation(firstname, lastName, zipCode) {
    await this.page.fill('id=first-name', firstname);
    await this.page.fill('id=last-name', lastName);
    await this.page.fill('id=postal-code', zipCode);
  }

  async clickContinue() {
    await this.page.click('id=continue');
  }
}

module.exports = { CheckoutPage };
