class Header {
  constructor(page) {
    this.page = page;
  }

  async clickHamburgerButton() {
    await this.page.click('id=react-burger-menu-btn');
  }

  async clickLogout() {
    await this.page.click('id=logout_sidebar_link');
  }
}

module.exports = { Header };
