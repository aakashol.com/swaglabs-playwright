const { exec } = require('child_process');
const fs = require('fs');

const myArgs = process.argv.slice(2);
const feature = myArgs[0].toLowerCase();
const environment = myArgs[1].toLowerCase();
const product = myArgs[2].toLowerCase();

/**
 * check for correct environment of a product
 */
function checkEnvironment() {
  const data = fs.readFileSync('product-environment-map.json');
  if (!JSON.parse(data)[product].includes(environment)) {
    console.error(`Incorrect environment: ${environment}`);
    process.exit(1);
  }
}

/**
 * check for correct feature of a product
 */
function checkFeature() {
  if (feature !== 'all') {
    const data = fs.readFileSync('product-features-map.json');
    if (!JSON.parse(data)[product].includes(feature)) {
      console.error(`Incorrect feature: ${feature}`);
      process.exit(1);
    }
  }
}

/**
 * check for correct product
 */
function checkProduct() {
  const data = fs.readFileSync('products.json');
  if (!JSON.parse(data).products.includes(product)) {
    console.error(`Incorrect product: ${product}`);
    process.exit(1);
  }
}

checkProduct();
checkEnvironment();
checkFeature();

const details = {
  environment,
  product,
  feature,
};

fs.writeFileSync('./temp/globals.json', JSON.stringify(details, null, 4));

if (feature === 'all') {
  exec(
    `npx mocha -r esm tests/${product}/**/*.spec.js --timeout 10000 --reporter mochawesome`,
    (error, stdout, stderr) => {
      if (error) {
        console.log(`error: ${error.message}`);
        return;
      }
      if (stderr) {
        console.log(`stderr: ${stderr}`);
        return;
      }
      console.log(`stdout: ${stdout}`);
    },
  );
} else {
  exec(
    `npx mocha -r esm tests/${product}/${feature}/*.spec.js --timeout 20000 --reporter mochawesome`,
    (error, stdout, stderr) => {
      if (error) {
        console.log(`error: ${error.message}`);
        return;
      }
      if (stderr) {
        console.log(`stderr: ${stderr}`);
        return;
      }
      console.log(`stdout: ${stdout}`);
    },
  );
}
